# Capstone_Space_Invaders

Este proyecto de Space Invaders es parte de la asignatura de Programación 3 y fue desarrollado en el entorno de desarrollo Visual Studio Code en Ubuntu. Utiliza el lenguaje de programación C# junto con el framework .NET, y está implementado utilizando el patrón arquitectónico MVVM (Model-View-ViewModel) y XAML para la interfaz de usuario.

## Descripción del Proyecto

Space Invaders es un clásico juego arcade en el que el jugador controla una nave espacial que debe defenderse de una horda de invasores alienígenas que se desplazan horizontalmente por la pantalla y que intentan destruir la nave del jugador disparando misiles. El objetivo del jugador es eliminar a todos los invasores antes de que lleguen al suelo y ganar la mayor cantidad de puntos posible.

## Características del Juego

- Nave Espacial Controlable: El jugador puede mover la nave espacial horizontalmente para esquivar los ataques de los invasores y disparar misiles para destruirlos.

- Invasores Alienígenas: Una horda de invasores alienígenas se desplaza horizontalmente por la pantalla, disparando misiles y descendiendo gradualmente hacia el suelo. El jugador debe eliminar a todos los invasores para ganar el juego.

- Puntuación y Niveles: El juego lleva un registro de la puntuación del jugador y aumenta la dificultad a medida que avanza de nivel, con invasores más rápidos y agresivos.

- Interfaz Gráfica de Usuario (GUI): El juego cuenta con una interfaz gráfica de usuario intuitiva y atractiva, desarrollada con XAML, que muestra la nave del jugador, los invasores alienígenas, la puntuación y otros elementos visuales.

## Detalles acerca del entorno de desarrollo

- Sistema Operativo: Ubuntu
- Entorno de Desarrollo: Visual Studio Code
- Lenguaje de Programación: C#
- Framework: .NET
- Patrón Arquitectónico: MVVM (Model-View-ViewModel)
- Interfaz de Usuario: XAML

## Instrucciones de Ejecución

1. Clonar el repositorio desde GitHub.

2. Abrir el proyecto en Visual Studio Code.

3. Compilar y ejecutar el proyecto.

4. Disfrutar del juego de Space Invaders y defender la Tierra de la invasión alienígena.

## Autor
Desarrollado por Lance Yapuchura Mamani
