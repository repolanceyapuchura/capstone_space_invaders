using System.Collections.ObjectModel;
using capstone_space_invaders.Models;
using Windows.Foundation;


namespace capstone_space_invaders;

public class InvaderManager
{
    public ObservableCollection<Invader> Invaders { get; private set; }
    public bool MoveRight { get => MoveRight1; set => MoveRight1 = value; }
    public bool MoveDown { get => MoveDown1; set => MoveDown1 = value; }

    public static int CanvasWidth1 => CanvasWidth2;

    public bool MoveRight1 { get => _moveRight; set => _moveRight = value; }
    public bool MoveDown1 { get => _moveDown; set => _moveDown = value; }

    public static int CanvasWidth2 => CanvasWidth;

    private bool _moveRight = true;
    private bool _moveDown = false;
    private const int CanvasWidth = 1000;
    public InvaderManager()
    {
        Invaders = new ObservableCollection<Invader>();
        InitializeInvaders();
    }

    private void InitializeInvaders()
    {
        double startX = (double)InvaderConstants.InvaderStartX;
        double startY = (double)InvaderConstants.InvaderStartY;

        for (int row = 0; row < (int)InvaderConstants.InvaderRows; row++)
        {
            for (int col = 0; col < (int)InvaderConstants.InvaderColumns; col++)
            {
                double x = startX + col * ((int)InvaderConstants.InvaderWidth + (double)InvaderConstants.InvaderHorizontalSpacing);
                double y = startY + row * ((int)InvaderConstants.InvaderHeight + (double)InvaderConstants.InvaderVerticalSpacing);
                Invaders.Add(new Invader(x, y, (int)InvaderConstants.InvaderSpeed));
            }
        }
    }

    public void MoveInvaders(double screenWidth, double screenHeight)
    {
        double lastLeftInvaderX = Invaders[0].X + (double)InvaderConstants.InvaderWidth / 2;
        double lastRightInvaderX = Invaders[Invaders.Count - 1].X + (double)InvaderConstants.InvaderWidth / 2;

        if ((MoveRight && lastRightInvaderX >= CanvasWidth1) || (!MoveRight && lastLeftInvaderX <= 0))
        {
            MoveRight = !MoveRight;

            MoveDown = true;
        }

        foreach (var invader in Invaders)
        {
            if (MoveRight)
            {
                invader.MoveRight();
            }
            else
            {
                invader.MoveLeft();
            }
        }

        if (MoveDown)
        {
            foreach (var invader in Invaders)
            {
                invader.Y += 10;
            }
            MoveDown = false;
        }

    }
}
