using System;
using Windows.Media.Core;
using Windows.Media.Playback;

namespace capstone_space_invaders.Services;

public class SoundService
{
    private MediaPlayer mediaPlayer;

    public SoundService()
    {
        MediaPlayer = new MediaPlayer();
    }

    public MediaPlayer MediaPlayer { get => mediaPlayer; set => mediaPlayer = value; }

    public void PlaySound(string soundPath)
    {
        MediaPlayer.Source = MediaSource.CreateFromUri(new Uri(soundPath));
        MediaPlayer.Play();
    }

    public void SetVolume(double volume)
    {
        MediaPlayer.Volume = volume;
    }
}