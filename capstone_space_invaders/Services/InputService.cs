using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Core;

namespace capstone_space_invaders.Services;

public class InputService
{
    private CoreDispatcher _dispatcher;

    public CoreDispatcher Dispatcher { get => _dispatcher; set => _dispatcher = value; }

    public event EventHandler<KeyEventArgs> KeyDown;

    public InputService(CoreDispatcher dispatcher)
    {
        Dispatcher = dispatcher;
        Window.Current.CoreWindow.KeyDown += CoreWindow_KeyDown;
    }

    private async void CoreWindow_KeyDown(CoreWindow sender, KeyEventArgs args)
    {
        await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
        {
            OnKeyDown(args);
        });
    }
    protected virtual void OnKeyDown(KeyEventArgs args)
    {
        KeyDown?.Invoke(this, args);
    }
}