using capstone_space_invaders.Services;
using capstone_space_invaders.ViewModels;
using Windows.UI.Core;

namespace capstone_space_invaders;

public sealed partial class MainPage : Page
{
    GameViewModel viewModel;

    public GameViewModel ViewModel { get => viewModel; set => viewModel = value; }

    public MainPage()
    {
        this.InitializeComponent();
        ViewModel = new GameViewModel(new InputService(CoreWindow.GetForCurrentThread().Dispatcher), new SoundService());
        this.DataContext = ViewModel;

        Loaded += MainPage_Loaded;
    }

    private void MainPage_Loaded(object sender, RoutedEventArgs e)
    {
        Window.Current.CoreWindow.KeyDown += CoreWindow_KeyDown;
    }

    private void CoreWindow_KeyDown(CoreWindow sender, KeyEventArgs args)
    {
        ViewModel.HandleKeyDown(sender, args);
    }
}
