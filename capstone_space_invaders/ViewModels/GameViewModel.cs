using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using capstone_space_invaders.Models;
using capstone_space_invaders.Services;
using Windows.System;
using Windows.UI.Core;
using Windows.Foundation;

namespace capstone_space_invaders.ViewModels;

public class GameViewModel : INotifyPropertyChanged
{
    private Ship _ship;
    private readonly InputService _inputService;
    private readonly SoundService _soundService;
    private readonly InvaderManager _invaderManager;

    public ObservableCollection<Invader> Invaders => InvaderManager.Invaders;
    private readonly DispatcherTimer _timer;
    public Ship Ship
    {
        get => Ship1;
        set
        {
            if (Ship1 != value)
            {
                Ship1 = value;
                OnPropertyChanged();
            }
        }
    }
    private ObservableCollection<Projectile> _projectiles;
    public ObservableCollection<Projectile> Projectiles
    {
        get => Projectiles1;
        set
        {
            if (Projectiles1 != value)
            {
                Projectiles1 = value;
                OnPropertyChanged();
            }
        }
    }
    private ObservableCollection<Ship> _lives;
    public ObservableCollection<Ship> Lives
    {
        get { return Lives1; }
        set
        {
            if (Lives1 != value)
            {
                Lives1 = value;
                OnPropertyChanged();
            }
        }
    }
    public double CanvasWidth { get; private set; }
    public double CanvasHeight { get; private set; }
    public Ship Ship1 { get => _ship; set => _ship = value; }

    public InputService InputService => _inputService;

    public SoundService SoundService => _soundService;

    public InvaderManager InvaderManager => _invaderManager;

    public DispatcherTimer Timer => _timer;

    public ObservableCollection<Projectile> Projectiles1 { get => _projectiles; set => _projectiles = value; }
    public ObservableCollection<Ship> Lives1 { get => _lives; set => _lives = value; }

    public GameViewModel(InputService inputService, SoundService soundService)
    {
        _inputService = inputService;
        _soundService = soundService;
        _invaderManager = new InvaderManager();

        _inputService.KeyDown += InputService_KeyDown;

        Ship = new Ship { X = 500, Y = 740 };
        Lives = new ObservableCollection<Ship>();
        AddInitialLives();
        InitializeGame();

        _timer = new DispatcherTimer();
        _timer.Interval = TimeSpan.FromMilliseconds(50);
        _timer.Tick += Timer_TickAsync;
        _timer.Start();

    }

    public event PropertyChangedEventHandler PropertyChanged;

    private void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    private void InitializeGame()
    {
        Projectiles = new ObservableCollection<Projectile>();

        Task.Run(async () =>
        {
            while (true)
            {
                UpdateProjectiles();
                InvaderManager.MoveInvaders(CanvasWidth, CanvasHeight);
                await Task.Delay(10);
            }
        });
    }
    private async void Timer_TickAsync(object sender, object e)
    {
        await Windows.ApplicationModel.Core.CoreApplication.MainView.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
        {
            UpdateProjectiles();
        });
    }

    private void InputService_KeyDown(object sender, KeyEventArgs args)
    {
        if (args.KeyStatus.RepeatCount == 1)
        {
            switch (args.VirtualKey)
            {
                case VirtualKey.Left:
                    MoveLeft(Ship);
                    break;

                case VirtualKey.Right:
                    MoveRight(Ship);
                    break;

                case VirtualKey.Space:
                    FireLaser();
                    break;
            }
        }
    }

    private void MoveLeft(Ship ship)
    {
        SoundService.SetVolume(1.0);
        SoundService.PlaySound("//Assets/sounds/move_sound.wav");
        if (ship.X - 6 >= 0)
            ship.X -= 8;
    }

    private void MoveRight(Ship ship)
    {
        SoundService.SetVolume(1.0);
        SoundService.PlaySound("//Assets/sounds/raygunShot.wav");
        if (ship.X + 6 + ship.Width <= 960)
            ship.X += 8;
    }

    private void FireLaser()
    {
        double projectileX = Ship.X + (Ship.Width / 2);
        double projectileY = Ship.Y;
        Projectiles.Add(new Projectile(projectileX, projectileY, 5));
    }

    private void UpdateProjectiles()
    {
        foreach (var projectile in Projectiles.ToList())
        {
            projectile.MoveUp();

            foreach (var invader in Invaders.ToList())
            {
                if (CheckCollision(projectile, invader))
                {
                    HandleCollision(projectile, invader);
                    break;
                }
            }

            if (projectile.Y < 0)
            {
                Projectiles.Remove(projectile);
            }
        }

        OnPropertyChanged(nameof(Projectiles));
    }

    private void AddInitialLives()
    {
        for (int i = 0; i < 3; i++)
        {
            Lives.Add(new Ship());
        }
    }
    private bool CheckCollision(Projectile projectile, Invader invader)
    {
        Rect projectileBounds = new Rect(projectile.X, projectile.Y, projectile.Width, projectile.Height);

        Rect invaderBounds = new Rect(invader.X, invader.Y, invader.Width, invader.Height);

        return (projectileBounds.Right > invaderBounds.Left &&
                projectileBounds.Left < invaderBounds.Right &&
                projectileBounds.Bottom > invaderBounds.Top &&
                projectileBounds.Top < invaderBounds.Bottom);
    }

    private void HandleCollision(Projectile projectile, Invader invader)
    {
        Projectiles.Remove(projectile);
        Invaders.Remove(invader);

        Ship.Points += 10;

        OnPropertyChanged(nameof(Ship.Points));
    }
    public void HandleKeyDown(CoreWindow sender, KeyEventArgs args)
    {
        InputService_KeyDown(sender, args);
    }
}