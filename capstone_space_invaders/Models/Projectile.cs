using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Windows.Foundation;

namespace capstone_space_invaders.Models;

public class Projectile
{
    private double _x;
    private double _y;
    private double _width;
    private double _height;
    private double _speed;

    public Rect Bounds => new Rect(X, Y, Width, Height);

    public double X
    {
        get { return X1; }
        set
        {
            X1 = value;
            OnPropertyChanged();
        }
    }

    public double Y
    {
        get { return Y1; }
        set
        {
            Y1 = value;
            OnPropertyChanged();
        }
    }

    public double Width
    {
        get { return Width1; }
        set
        {
            if (Width1 != value)
            {
                Width1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double Height
    {
        get { return Height1; }
        set
        {
            if (Height1 != value)
            {
                Height1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double Speed
    {
        get { return Speed1; }
        set
        {
            if (Speed1 != value)
            {
                Speed1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double X1 { get => _x; set => _x = value; }
    public double Y1 { get => _y; set => _y = value; }
    public double Width1 { get => _width; set => _width = value; }
    public double Height1 { get => _height; set => _height = value; }
    public double Speed1 { get => _speed; set => _speed = value; }

    public Projectile(double x, double y, double speed)
    {
        X = x;
        Y = y;
        Speed = speed;
        Width = 10; // Ajusta el ancho según tus necesidades
        Height = 20; // Ajusta el alto según tus necesidades
    }

    public void MoveUp()
    {
        Y -= Speed;
    }

    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}