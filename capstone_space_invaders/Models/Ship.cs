using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.Foundation;

namespace capstone_space_invaders.Models;

public class Ship : INotifyPropertyChanged
{
    private double _x;
    private double _y;
    private double _width;
    private double _height;
    private int _lives;
    private int _points;

    public Rect Bounds => new Rect(X, Y, Width, Height);

    public int Points
    {
        get { return Points1; }
        set
        {
            if (Points1 != value)
            {
                Points1 = value;
                OnPropertyChanged(nameof(Points));
            }
        }
    }
    public double X
    {
        get { return X1; }
        set
        {
            if (X1 != value)
            {
                X1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double Y
    {
        get { return Y1; }
        set
        {
            if (Y1 != value)
            {
                Y1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double Width
    {
        get { return Width1; }
        set
        {
            if (Width1 != value)
            {
                Width1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double Height
    {
        get { return Height1; }
        set
        {
            if (Height1 != value)
            {
                Height1 = value;
                OnPropertyChanged();
            }
        }
    }

    public int Lives
    {
        get { return Lives1; }
        set
        {
            if (Lives1 != value)
            {
                Lives1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double X1 { get => _x; set => _x = value; }
    public double Y1 { get => _y; set => _y = value; }
    public double Width1 { get => _width; set => _width = value; }
    public double Height1 { get => _height; set => _height = value; }
    public int Lives1 { get => _lives; set => _lives = value; }
    public int Points1 { get => _points; set => _points = value; }

    public Ship()
    {
        X = 0;
        Y = 0;
        Width = 50;
        Height = 50;
        Lives = 3;
    }

    // Implementación de INotifyPropertyChanged
    public event PropertyChangedEventHandler PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}