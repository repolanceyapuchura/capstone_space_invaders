using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.Foundation;

namespace capstone_space_invaders.Models;

public class Invader : INotifyPropertyChanged
{
    private double _x;
    private double _y;
    private double _speed;

    public double Width { get; set; }
    public double Height { get; set; }
    public double Speed
    {
        get { return Speed1; }
        set
        {
            if (Speed1 != value)
            {
                Speed1 = value;
                OnPropertyChanged();
            }
        }
    }

    // Propiedad de solo lectura para los límites del invasor
    public Rect Bounds => new Rect(X, Y, Width, Height);

    public double X
    {
        get { return X1; }
        set
        {
            if (X1 != value)
            {
                X1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double Y
    {
        get { return Y1; }
        set
        {
            if (Y1 != value)
            {
                Y1 = value;
                OnPropertyChanged();
            }
        }
    }

    public double X1 { get => _x; set => _x = value; }
    public double Y1 { get => _y; set => _y = value; }
    public double Speed1 { get => _speed; set => _speed = value; }

    public Invader(double x, double y, double speed)
    {
        X = x;
        Y = y;
        Speed = speed;
    }

    public void MoveDown(double distance)
    {
        Y += distance;
    }

    public void MoveLeft()
    {
        X -= Speed;
    }

    public void MoveRight()
    {
        X += Speed;
    }

    public event PropertyChangedEventHandler? PropertyChanged;

    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
