namespace capstone_space_invaders;

public enum InvaderConstants
{
    InvaderWidth = 20,
    InvaderHeight = 20,
    InvaderRows = 5,
    InvaderColumns = 8,
    InvaderSpeed = 2,
    InvaderHorizontalSpacing = 30,
    InvaderVerticalSpacing = 15,
    InvaderStartX = 25,
    InvaderStartY = 35
}
